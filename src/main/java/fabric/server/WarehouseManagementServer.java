package fabric.server;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import fabric.WarehouseManagementSystem;
import io.javalin.Javalin;

public class WarehouseManagementServer {
    private Gson gson;
    private final Javalin server;
    private final int port;
    private final WarehouseManagementSystem warehouseManagementSystem;

    public WarehouseManagementServer(int port) {
        this.port = port;
        server = Javalin.create();
        warehouseManagementSystem = new WarehouseManagementSystem();
        gson = new Gson();
    }

    public void start() {
        server.start(port);
        initRoutes();
    }

    private void initRoutes() {
        server.post("/order", context -> {
            String[] orders = gson.fromJson(context.queryParam("order"), String[].class);
            warehouseManagementSystem.orderStock(orders);
        });
        server.post("/supply", context -> {
            String[] supply = gson.fromJson(context.queryParam("supply"), String[].class);
            warehouseManagementSystem.supplyStock(supply);
        });
        server.post("/tasks/{id}/complete", context -> {
            warehouseManagementSystem.completeTask(context.pathParam("id"));
        });
        server.get("/next-tasks", context -> {
            context.result(warehouseManagementSystem.getTasks().toString());
        });
        server.get("/stock", context -> {
            context.result(warehouseManagementSystem.getStock().toString());
        });
    }
}
