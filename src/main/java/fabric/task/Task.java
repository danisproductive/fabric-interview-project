package fabric.task;

import java.util.UUID;

public class Task {
    private String id;
    private TaskAction action;
    private String productName;

    public Task(TaskAction action, String productName) {
        this.id = UUID.randomUUID().toString();;
        this.action = action;
        this.productName = productName;
    }

    public TaskAction getAction() {
        return action;
    }

    public String getProductName() {
        return productName;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=\"" + id + "\"" +
                ", action=\"" + action.getValue() + "\"" +
                ", productName=\"" + productName + "\"" +
                '}';
    }
}
