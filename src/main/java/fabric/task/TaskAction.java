package fabric.task;

public enum TaskAction {
    SUPPLY("put_to_stock"), ORDER("pick_from_stock");

    private String value;

    TaskAction(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
