package fabric.task;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TasksQueue {
    private final Map<String, Task> tasks;

    public TasksQueue() {
        tasks = new HashMap<>();
    }

    public void put(Task task) {
        tasks.put(task.getId(), task);
    }

    public Task pop(String taskId) {
        Task requiredTask = tasks.get(taskId);

        if (requiredTask != null) {
            tasks.remove(taskId);
        }

        return requiredTask;
    }

    public List<Task> getCurrentTasks() {
        return tasks.values().stream().toList();
    }
}
