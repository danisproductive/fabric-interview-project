package fabric;

import fabric.stock.Stock;
import fabric.stock.WarehouseStock;
import fabric.task.Task;
import fabric.task.TaskAction;
import fabric.task.TasksQueue;

import java.util.Arrays;
import java.util.List;

public class WarehouseManagementSystem {
    private WarehouseStock stock;
    private TasksQueue tasksQueue;

    public WarehouseManagementSystem() {
        stock = new WarehouseStock();
        tasksQueue = new TasksQueue();
    }

    public void supplyStock(String[] newSupply) {
        Arrays.stream(newSupply).forEach(supplyItem -> tasksQueue.put(new Task(TaskAction.SUPPLY, supplyItem)));
    }


    public void orderStock(String[] supplyToTake) {
        Arrays.stream(supplyToTake).forEach(supplyItem -> tasksQueue.put(new Task(TaskAction.ORDER, supplyItem)));
    }

    public List<Stock> getStock() {
        return stock.getItemStocks().values().stream().toList();
    }

    public List<Task> getTasks() {
        return tasksQueue.getCurrentTasks();
    }

    public void completeTask(String id) {
        Task task = tasksQueue.pop(id);
        String productName = task.getProductName();
        TaskAction action = task.getAction();

        if (action == TaskAction.ORDER) {
            stock.takeFromStock(productName, 1);
        } else {
            if (action == TaskAction.SUPPLY) {
                stock.putToStock(productName, 1);
            }
        }
    }
}
