package fabric.stock;

import java.util.HashMap;
import java.util.Map;

public class WarehouseStock {
    private Map<String, Stock> itemStocks;

    public WarehouseStock() {
        itemStocks = new HashMap<>();
        initStock();
    }

    public void takeFromStock(String productName, int amount) {
        Stock productStock = itemStocks.get(productName);

        if (productStock == null) {
            System.out.println("No such stock: " + productName);
        } else {
            boolean isValidAction = productStock.takeFromStock(amount);

            if (!isValidAction) {
                System.out.println("Could not perform action, not enough of " + productName);
            }
        }
    }

    public void putToStock(String productName, int amount) {
        Stock productStock = itemStocks.get(productName);

        if (productStock == null) {
            System.out.println("No such stock: " + productName);
        } else {
            productStock.putToStock(amount);
        }
    }

    public Stock getItemStock(String itemName) {
        return itemStocks.get(itemName);
    }

    public Map<String, Stock> getItemStocks() {
        return itemStocks;
    }

    private void initStock() {
        itemStocks.put("Milk", new Stock("Milk", new int[] {0, 0}, 10));
        itemStocks.put("Bread", new Stock("Bread", new int[] {0, 1}, 10));
        itemStocks.put("Soap", new Stock("Soap", new int[] {0, 2}, 10));
        itemStocks.put("Salt", new Stock("Salt", new int[] {0, 3}, 10));
        itemStocks.put("Pasta", new Stock("Pasta", new int[] {0, 4}, 10));
    }
}
