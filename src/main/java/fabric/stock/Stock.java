package fabric.stock;

import java.util.Arrays;

public class Stock {
    private String name;
    private int[] location;
    private int currentAmount;

    public Stock(String name, int[] location, int currentAmount) {
        this.name = name;
        this.location = location;
        this.currentAmount = currentAmount;
    }

    public Stock(int[] location, int currentAmount) {
        this.location = location;
        this.currentAmount = currentAmount;
    }

    public boolean takeFromStock(int amount) {
        if (amount <= currentAmount) {
            currentAmount -= amount;

            return true;
        }

        return false;
    }

    public void putToStock(int amount) {
        currentAmount += amount;
    }

    @Override
    public String toString() {
        return "{" +
                "name:\"" + name + "\"" +
                ", amount:" + currentAmount +
                '}';
    }
}
