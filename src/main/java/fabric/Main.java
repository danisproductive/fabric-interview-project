package fabric;

import fabric.server.WarehouseManagementServer;
import io.javalin.Javalin;

public class Main {
    public static void main(String[] args) {
        WarehouseManagementServer server = new WarehouseManagementServer(7000);
        server.start();
    }
}
